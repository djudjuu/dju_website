import { Box, Text, Link, Flex, Wrap, VStack, HStack } from "@chakra-ui/react";

const SourceLink = ({ key, href }) => (
  <Link href={href} isExternal>
    <Box>
      <Text as="kbd">{key || "{view source}"}</Text>
    </Box>
  </Link>
);

export const Credits = ({ people, sources }) => {
  return (
    <Box
      // w="full"
      // display="flex"
      // flexDirection="row-reverse"
      textAlign="right"
      // justifyContent="flex-end"
      // alignItems="right"
    >
      {people && (
        <Text>
          <Text as="i">Credits: </Text>
          {people.join(", ")}
        </Text>
      )}
      {sources &&
        (Object.keys(sources).length === 1 ? (
          <SourceLink href={Object.keys(sources)[0]} />
        ) : (
          <Box>
            <Text as="kbd"> {"{View source: "} </Text>
            {Object.keys(sources).map((key, index) => (
              <>
                {index === 0 && <Text as="kbd">[</Text>}
                <Link href={sources[key]} isExternal>
                  <Text as="kbd">{key}</Text>
                </Link>
                {index < Object.keys(sources).length - 1 ? ",  " : "] }"}
              </>
            ))}
          </Box>
        ))}
    </Box>
  );
};

// {/* if sources has only one key, then show it as a link
//   else show all keys as links */}
{
  /* {Object.entries(sources).map(([key, value]) => ( <SourceLink key={key} href={value} />
  ))} */
}
