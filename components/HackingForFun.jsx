import {
  Center,
  VStack,
  Image,
  Link,
  Heading,
  Drawer,
  Text,
  Accordion,
  ListItem,
  Box,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  UnorderedList,
} from "@chakra-ui/react";
import { Credits } from "./Credits";
import dynamic from "next/dynamic";

const EmbeddedVideo = dynamic(() => import("./EmbeddedVideo"), { ssr: false });

const HackingForFun = () => {
  return (
    <AccordionItem>
      <h2>
        <AccordionButton>
          <Box flex="1" textAlign="left">
            <Heading as="h4"> Personal Side Projects</Heading>
          </Box>
          <AccordionIcon />
        </AccordionButton>
      </h2>
      <AccordionPanel pb={4}>
        <Text>
          <Link
            href="https://coolguy.website/the-future-will-be-technical/"
            isExternal
            color="purple.500"
          >
            {" "}
            The future is technical.{" "}
          </Link>
          And while I see coding as my profession, I also do it as a hobby.
        </Text>
        <UnorderedList>
          <br />
          <ListItem>
            <Text as="b">A book recommendation engine</Text> for my friends and
            familiy so that they can browse my favorites and get recommendations
            based on what they are looking for. I plan to extend this to allow
            anyone to create their own bookshelf and to include tags, reads,
            comments, etc in the future.
            <Link href="books.djuju.tech" color="purple.500">
              {" "}
              Go check it out!{" "}
            </Link>
            <br />
            (Also, I want to implement this with a different stack to dive into
            svelte and learn more GraphQL)
            <Image
              src="./images/djinni.png"
              alt="djinni recommendation engine"
            />
            <Credits
              people={["raio"]}
              sources={{
                backend: "https://github.com/djudjuu/BookDjinni",
                frontend:
                  "https://github.com/djudjuu/book-djinni-frontend-nextjs",
              }}
            />
          </ListItem>
          <br />
          <ListItem>
            I like to play around with Microcontrollers and LEDs. Coding so
            close to the metal (C++) and actually having to worry whether my
            trippy LED animation will fit in memory is great.
            <Center
              w={{ base: "100%", md: "80%" }}
              h={{ base: "100%", md: "80%" }}
              mt={4}
            >
              <EmbeddedVideo path={"./videos/lamp.mp4"} />
            </Center>
            <Credits
              people={["Sebastian Schmid, Felix"]}
              sources={{
                code: "https://github.com/djudjuu/LEDVisuals",
              }}
            />
          </ListItem>
        </UnorderedList>
      </AccordionPanel>
    </AccordionItem>
  );
};

export default HackingForFun;
