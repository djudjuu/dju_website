import {
  Center,
  Link,
  VStack,
  Heading,
  Drawer,
  Text,
  Accordion,
  ListItem,
  Box,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  UnorderedList,
} from "@chakra-ui/react";
import { Credits } from "./Credits";

const SmartContracts = () => {
  return (
    <AccordionItem>
      <h2>
        <AccordionButton>
          <Box flex="1" textAlign="left">
            <Heading as="h4"> Smart Contracts</Heading>
          </Box>
          <AccordionIcon />
        </AccordionButton>
      </h2>
      <AccordionPanel pb={4}>
        <UnorderedList>
          <br />
          <ListItem>
            <Text as="b">Smart Contract Assessment system</Text> for fathom.org
            consisting of
            <UnorderedList>
              <ListItem>
                {" "}
                a concept-tree to organize related areas of knowledge{" "}
              </ListItem>
              <ListItem>
                a staked schelling game to determine who should get a
                certificate (aka become member of a concept)
              </ListItem>
              <ListItem>
                An ERC-20 Token that would be minted (or burned) by completing
                assessment-games (nerd-summary!
              </ListItem>
            </UnorderedList>
            Check out{" "}
            <Link
              href="https://docs.fathom.network/en/latest/"
              isExternal
              color="purple.500"
            >
              our whitepaper
            </Link>{" "}
            for a more thorough explanation.
            <Credits
              people={["Jared Pereira"]}
              sources={{ code: "https://gitlab.com/fathom/assess" }}
            />
          </ListItem>
          <br />
          <ListItem>
            Contributions to {""}
            <Text as="b">{"Ethereum's"} solidity compiler </Text> (C++): I
            completed a a new way of exporting the Abstract Syntax Tree (AST) of
            a program to a JSON-file and also added an import-from-json feature,
            that allows to re-import the AST into the compiler. This is useful
            for tools that want to manipulate code at a more syntactic level
            e.g. a mutation testing frameworks that wants to change a random
            comparison operator and see whether the test-suite catches it.
            <Credits
              people={["Christian Reitwiessner, Alex Beregszaszi"]}
              sources={{
                export: "https://github.com/ethereum/solidity/pull/7537",
                ["ast-import"]:
                  "https://github.com/ethereum/solidity/pull/7537",
              }}
            />
          </ListItem>
          <br />
          <ListItem>
            A <Text as="b">Snapshotted Voting Tool</Text> for equity-tokens.
            Token-holders can vote on proposals proportional to their balance at
            the time of the proposal submission.
            <Credits
              people={["Marcin Rudolf"]}
              sources={{
                code: "https://github.com/Neufund/platform-contracts/pull/310",
              }}
            />
          </ListItem>
        </UnorderedList>
      </AccordionPanel>
    </AccordionItem>
  );
};

export default SmartContracts;
