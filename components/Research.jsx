import {
  Center,
  VStack,
  Image,
  Link,
  Heading,
  Drawer,
  Text,
  Accordion,
  ListItem,
  Box,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  UnorderedList,
} from "@chakra-ui/react";
import { Credits } from "./Credits";
import dynamic from "next/dynamic";

// const LampVideo = dynamic(() => import("./LampVideo"), { ssr: false });
const EmbeddedVideo = dynamic(() => import("./EmbeddedVideo"), { ssr: false });

const HackingForFun = () => {
  return (
    <AccordionItem>
      <h2>
        <AccordionButton>
          <Box flex="1" textAlign="left">
            <Heading as="h4"> Research</Heading>
          </Box>
          <AccordionIcon />
        </AccordionButton>
      </h2>
      <AccordionPanel pb={4}>
        <Text>
          {" "}
          I {"didn't"} spend a lot of time doing actual research as I left Uni
          right after my masters, but I did one thing that {"I'm"} proud of. In{" "}
          <Link
            href="/files/thesis.pdf"
            alt="download pdf of thesis"
            target="_blank"
            rel="noopener noreferrer"
            color="purple.500"
          >
            my thesis
          </Link>{" "}
          I explored how to identify interesting directions for evolutionary
          algorithms when controlling robots. The overarching problem was that
          when a task is really difficult (e.g. walking), then the first steps
          to solving it (e.g. maintaining balance) do not necessarily look like
          the final solution. Broadly speaking, my thesis was that{" "}
          <Text as="b">
            diversity is interesting and entropy is useful to measure it
          </Text>
          .
          <br />
          <br />
          So without further ado, here is a video of robots trying to walk:
          <Center
            w={{ base: "100%", md: "80%" }}
            h={{ base: "100%", md: "80%" }}
            mt={4}
          >
            <EmbeddedVideo path={"./videos/robots.mp4"} />
          </Center>
          <Credits
            people={["Joel Lehman, Paulo Urbano, Oliver Brock"]}
            sources={{ code: "https://github.com/djudjuu/mazerobot-python" }}
          />
        </Text>
      </AccordionPanel>
    </AccordionItem>
  );
};

export default HackingForFun;
