import {
  Text,
  VStack,
  Button,
  Box,
  Flex,
  Center,
  Image,
  Spacer,
  Wrap,
  WrapItem,
} from "@chakra-ui/react";
import Head from "next/head";
import { useColorMode } from "@chakra-ui/react";
import { MoonIcon, SunIcon } from "@chakra-ui/icons";
import PageLink from "./PageLink";
import { Link } from "@chakra-ui/react";

const Layout = ({ children }) => {
  const { colorMode, toggleColorMode } = useColorMode();
  return (
    <div>
      <Head>
        <title>Djuju.tech</title>
        <meta name="Julius Faber's web presence" content="" />
        {/* <link rel="icon" href="/favicon.ico" /> */}
      </Head>
      <Flex
        justifyContent="space-between"
        alignItems="center"
        m={2}
        // minWidth="max-content"
        maxWidth={{ base: "100%", md: "60%" }}
        margin="auto"
      >
        <PageLink href="/">
          <Image
            src="./images/banana.png"
            alt="logo"
            width="40px"
            height="20px"
            borderRadius="full"
          />
        </PageLink>
        <Flex justifyContent="space-around" alignItems="center" m={2} gap={3}>
          <PageLink href="/projects">My Projects</PageLink>
          <PageLink href="/about">About</PageLink>
          <Button onClick={toggleColorMode} bg="transparent">
            {" "}
            {colorMode === "light" ? <MoonIcon /> : <SunIcon />}
          </Button>
        </Flex>
      </Flex>
      <Box minH="90vH">{children}</Box>
      <Center>
        <footer>
          <VStack h="3px" mt="14px" fontSize="sm">
            <Box>
              made with{" "}
              <Link href="https://nextjs.org/" isExternal color="purple.500">
                Next.JS,
              </Link>{" "}
              <Link
                href="https://www.club-mate.de/"
                isExternal
                color="purple.500"
              >
                Mate
              </Link>
              {" and "}
              <PageLink href="/magic">
                {/* <a>Magic</a> */}
                Magic
              </PageLink>
            </Box>
            <Link href="https://gitlab.com/djudjuu/dju_website/" isExternal>
              <Box>
                <Text as="kbd">{"{view source}"}</Text>
              </Box>
            </Link>
          </VStack>
        </footer>
      </Center>
    </div>
  );
};

export default Layout;
