import {
  Center,
  VStack,
  Image,
  Link,
  Heading,
  Drawer,
  Text,
  Accordion,
  ListItem,
  Box,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  UnorderedList,
  Circle,
} from "@chakra-ui/react";
import ReactPlayer from "react-player";
import { ArrowRightIcon } from "@chakra-ui/icons";

const EmbeddedVideo = ({ path }) => {
  return (
    <Box>
      <ReactPlayer
        url={path}
        width="100%"
        height="100%"
        controls={true}
        playing={true}
        light={true}
        playIcon={
          <Circle size="40px" bg="purple.500">
            {" "}
            <ArrowRightIcon />
          </Circle>
        }
      />
    </Box>
  );
};

export default EmbeddedVideo;
