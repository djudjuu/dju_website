import { ExternalLinkIcon } from "@chakra-ui/icons";
import {
  Center,
  Link,
  VStack,
  Heading,
  Drawer,
  Text,
  Accordion,
  ListItem,
  Box,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  UnorderedList,
} from "@chakra-ui/react";
import { Credits } from "./Credits";
import dynamic from "next/dynamic";

const EmbeddedVideo = dynamic(() => import("./EmbeddedVideo"), { ssr: false });

const FullStackStuff = () => {
  // const [isMobile] = useMediaQuery("(max-width: 768px)");
  return (
    <AccordionItem>
      <h2>
        <AccordionButton>
          <Box flex="1" textAlign="left">
            <Heading as="h4"> Full Stack Stuff</Heading>
          </Box>
          <AccordionIcon />
        </AccordionButton>
      </h2>
      <AccordionPanel pb={4}>
        Here are some projects I worked on during my time with arboreum.dev
        <UnorderedList>
          <br />
          <ListItem>
            <Text as="b">Testing our loan scheduling algorithm </Text>
            (math-heavy scipy-calculations) with pytest and serving it over an
            API with fastapi
            <Credits people={["Gaurav Singhal, Lawrence E. Day"]} />
          </ListItem>
          <br />
          <ListItem>
            A react & typescript frontend and a GraphQL backend for a{" "}
            <Text as="b">credit-union as a service platform.</Text> Activities
            included onboarding, requesting loans, and participating as a lender
            in a lending pool. Check out this demo video:
            <br />
            <br />
            <br />
            <Box w="80%" m="auto">
              <EmbeddedVideo path={"./videos/demo.mp4"} />
            </Box>
            <Credits
              people={["Gaurav Paruthi"]}
              sources={{
                code: "https://github.com/ArboreumDev/credit-union-frontend",
              }}
            />
          </ListItem>
          <br />
          <ListItem>
            A simple backend & smart-contracts to{" "}
            <Text as="b">capture loan-data on the Algorand Chain,</Text> by
            creating an Algorand-Standard-Asset according to ARC-3 standard for
            each loan and logging corresponding repayment data. Also, I
            integrated our credit union platform with Circle APIs to allow FIAT
            deposits and withdrawals. and used the AlgoConnect Wallet to allow
            USDC deposits from both Ethereum & Algorand.
          </ListItem>
          <Credits
            sources={{
              backend: "https://github.com/ArboreumDev/algo-loan-logger",
              ["smart-contracts"]:
                "https://github.com/ArboreumDev/algorand-credit",
              asset: "https://algoexplorer.io/asset/435928758",
            }}
          />
          <br />
          <ListItem>
            A front- and backend for <Text as="b">invoice financing pilot</Text>{" "}
            in rural India, to allow selected small business to extend
            credit-lines to their best customers. Credit was given against
            shipments of goods, which we verified and tracked by integrating
            with our logistics {"company's"} API.
          </ListItem>
          <Credits
            people={["Gaurav Singhal, Vishal Hemrajani"]}
            sources={{
              frontend:
                "https://github.com/ArboreumDev/invoice-finance-pilot-frontend",
              backend:
                "https://github.com/ArboreumDev/invoice-finance-pilot-backend",
            }}
          />
        </UnorderedList>
      </AccordionPanel>
    </AccordionItem>
  );
};

export default FullStackStuff;
