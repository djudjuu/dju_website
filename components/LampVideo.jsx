import {
  Center,
  VStack,
  Image,
  Link,
  Heading,
  Drawer,
  Text,
  Accordion,
  ListItem,
  Box,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  UnorderedList,
  Circle,
} from "@chakra-ui/react";
import ReactPlayer from "react-player";
import { Credits } from "./Credits";
import { ArrowRightIcon } from "@chakra-ui/icons";

const LampVideo = () => {
  return (
    <Box>
      <ReactPlayer
        url="./videos/lamp.mp4"
        width="100%"
        height="100%"
        controls={true}
        playing={true}
        // light={true}
        playIcon={
          <Circle size="40px" bg="purple.500">
            {" "}
            <ArrowRightIcon />
          </Circle>
        }
      />
    </Box>
  );
};

export default LampVideo;
