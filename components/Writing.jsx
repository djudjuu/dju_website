import {
  Center,
  VStack,
  Image,
  Link,
  Heading,
  Drawer,
  Text,
  Accordion,
  ListItem,
  Box,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  UnorderedList,
  List,
} from "@chakra-ui/react";

const Writing = () => {
  return (
    <AccordionItem>
      <h2>
        <AccordionButton>
          <Box flex="1" textAlign="left">
            <Heading as="h4"> Writing</Heading>
          </Box>
          <AccordionIcon />
        </AccordionButton>
      </h2>
      <AccordionPanel pb={4}>
        <Text>
          {" "}
          I feel most comfortable writing technically or scientifically, but can
          also write an occasional blog post or article that are meant to be
          accessable to a wider audience. Check
          <UnorderedList mt={4}>
            <ListItem>
              {" "}
              <Link
                href="/files/thesis.pdf"
                alt="download pdf of thesis"
                target="_blank"
                rel="noopener noreferrer"
                color="purple.500"
              >
                my thesis
              </Link>{" "}
              for an example of scientific writing.
            </ListItem>
            <ListItem>
              the{" "}
              <Link
                isExternal
                color="purple.500"
                href="https://docs.fathom.network/en/latest/"
              >
                documentation{" "}
              </Link>
              of this blockchain education platform for an example of technical
              writing.
            </ListItem>
            <ListItem>
              my{" "}
              <Link
                isExternal
                color="purple.500"
                href="https://medium.com/fathom-network/a-singleton-for-certificates-1db5d278e7f9"
              >
                medium{" "}
              </Link>
              account for some blog examples.
            </ListItem>
          </UnorderedList>
        </Text>
      </AccordionPanel>
    </AccordionItem>
  );
};

export default Writing;
