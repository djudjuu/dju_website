// two ways to embed youtube videos
//  one adds trackers to the page and the other doesn't display nicely (and probably also adds trackers)

// import React from "react";
// import YouTube from "react-youtube";
// import Plyr from "plyr-react";
// // import "plyr-react/dist/plyr.css"; => how to find this

// const demoVideoId = "iN3Bl4Xchwg";
// const videoSrc = {
//   type: "video",
//   sources: [
//     {
//       // src: "yWtFb9LJs3o",
//       provider: "youtube",
//       src: demoVideoId,
//     },
//   ],
// };

// // weird display
// const DemoVideo2 = () => {
//   return <Plyr source={videoSrc} />;
// };

// // includes tracker
// const DemoVideo = ({ w, h }) => {
//   const opts = {
//     height: h,
//     width: w,
//     playerVars: {
//       autoplay: 0,
//     },
//   };

//   return <YouTube videoId={demoVideoId} opts={opts} />;
// };

// export default DemoVideo;
