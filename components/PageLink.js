import NextLink from "next/link";
import { Link } from "@chakra-ui/react";

const PageLink = ({ href, children, color }) => (
  <NextLink href={href} passHref>
    <Link color={color || undefined}>{children}</Link>
  </NextLink>
);

export default PageLink;
