import Layout from "../components/Layout";
import { Center, Box, Image } from "@chakra-ui/react";

const Magic = () => {
  return (
    <Layout>
      <Center alignItems="center">
        <Image src="./images/magic.gif" alt="magic" borderRadius="full" />
      </Center>
    </Layout>
  );
};

export default Magic;
