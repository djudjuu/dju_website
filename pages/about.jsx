import Layout from "../components/Layout";
import PageLink from "../components/PageLink";
import {
  Text,
  UnorderedList,
  ListItem,
  Heading,
  VStack,
  Box,
  Center,
  Image,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  Link,
  Spacer,
} from "@chakra-ui/react";
import NextLink from "next/link";

const About = () => {
  return (
    <Layout>
      <Box>
        <Center>
          <VStack width={["80%", "50%"]}>
            <Box>
              <Text fontSize="xl"> ...this page is about me 😀 </Text>
              <br />
              <Text>
                You can read a bit about some things that are important to me
                below or you could go and check my{" "}
                <Link
                  href="https://github.com/djudjuu"
                  isExternal
                  color="purple.500"
                >
                  GitHub-
                </Link>{" "}
                or{" "}
                <Link
                  color="purple.500"
                  href="https://gitlab.com/djudjuu"
                  isExternal
                >
                  GitLab
                </Link>
                {"-Account or "}
                <Link
                  href="/files/cv.pdf"
                  alt="pdf download of CV"
                  target="_blank"
                  rel="noopener noreferrer"
                  color="purple.500"
                >
                  download my CV
                </Link>
              </Text>
              <br />
              <br />
              <Accordion allowToggle>
                <AccordionItem>
                  <h2>
                    <AccordionButton>
                      <Box flex="1" textAlign="left">
                        <Heading as="h4"> Learning</Heading>
                      </Box>
                      <AccordionIcon />
                    </AccordionButton>
                  </h2>
                  <AccordionPanel pb={4}>
                    The red thread through my academic life has been learning:
                    Teaching Math to pre-school children during High-school,
                    studying Neuroscience during my bachelor, ML & robotics
                    during my master (
                    <Link
                      href="/files/thesis.pdf"
                      alt="download pdf of thesis"
                      target="_blank"
                      rel="noopener noreferrer"
                      color="purple.500"
                    >
                      my thesis
                    </Link>{" "}
                    ) summarizing non-fiction books for blinkist in parallel. I
                    am awed by the brain and I like to challenge assumptions of
                    what people can learn at what age. I believe I can explain
                    everything to everyone ({"->"} just got to start at the
                    right entry point). I also like to think of myself as really
                    good in charades :)
                    <br />
                    <br />
                    Some big inspirations for me are
                    <UnorderedList>
                      <ListItem>
                        Salman Khan (especially
                        <Link
                          color="purple.500"
                          href="https://www.ted.com/talks/sal_khan_let_s_use_video_to_reinvent_education?language=en"
                          isExternal
                        >
                          {" this talk"}
                        </Link>
                        )
                      </ListItem>
                      <ListItem>
                        Sugata {"Mitra's"}
                        <Link
                          color="purple.500"
                          href="http://www.hole-in-the-wall.com/"
                          isExternal
                        >
                          {" hole in the wall project"}
                        </Link>
                        )
                      </ListItem>
                      <ListItem>
                        my former colleague and friend
                        <Link color="purple.500" href="https://awarm.space/">
                          {" Jared Pereira"}
                        </Link>{" "}
                        and
                      </ListItem>
                      <ListItem>
                        <Link
                          color="purple.500"
                          href="https://waitbutwhy.com/"
                          isExternal
                        >
                          {"WaitbutWhy "}
                        </Link>
                        (everything can be explained!)
                      </ListItem>
                    </UnorderedList>
                  </AccordionPanel>
                </AccordionItem>

                <AccordionItem>
                  <h2>
                    <AccordionButton>
                      <Box flex="1" textAlign="left">
                        <Heading as="h4"> Decentralization</Heading>
                      </Box>
                      <AccordionIcon />
                    </AccordionButton>
                  </h2>
                  <AccordionPanel pb={4}>
                    In 2017, I came across Ethereum and since am a firm believer
                    that the world needs technology that is not owned by anyone.
                    So I put my first love (learning) to the side and dove into
                    different web3-projects (the solidty-compiler, fathom &
                    airswap at Consensys), neufund.org) and most recently tried
                    to use blockchains to super-charge the informal credit
                    landscape in India as a co-founder of
                    <Link
                      color="purple.500"
                      href="https://arboreum.dev/"
                      isExternal
                    >
                      {" arboreum.dev"}
                    </Link>
                    . In the end, that {"didn't"} quite work out, yet I learned
                    a lot and am proud of the change we made.
                  </AccordionPanel>
                </AccordionItem>

                <AccordionItem>
                  <h2>
                    <AccordionButton>
                      <Box flex="1" textAlign="left">
                        <Heading as="h4"> Developing Experience </Heading>
                      </Box>
                      <AccordionIcon />
                    </AccordionButton>
                  </h2>
                  <AccordionPanel pb={4}>
                    I like optimizing my
                    <Link color="purple.500" href="/setup">
                      {" setup "}
                    </Link>
                    & editors (recently fell in love with the Github Co-Pilot),
                    readable testable elegant code, paircoding dynamics, small
                    PRs that are fun to review, good issue descriptions,
                    tag-team-debugging, hands-dirty down to the root of the
                    problem! And, very importantly: Tests tests tests and then
                    more tests.
                  </AccordionPanel>
                </AccordionItem>

                <AccordionItem>
                  <h2>
                    <AccordionButton>
                      <Box flex="1" textAlign="left">
                        <Heading as="h4"> Team Dynamics</Heading>
                      </Box>
                      <AccordionIcon />
                    </AccordionButton>
                  </h2>
                  <AccordionPanel pb={4}>
                    Everyone being a novice at one thing and an expert at a
                    different one is what makes being part of a dev-team so much
                    fun to me. I enjoy sharing what I know and learning new
                    things to equal parts and I think {"it's"} part of what I
                    love best about being a developer.
                    <br />
                    <br />
                    To that end: I care about (in no particular order):
                    <br />
                    Smooth async workflow, showing appreciation for work well
                    done, direct and open communication about problems, honesty,
                    retros, productive meetings: being on time (very german
                    here), agenda-setting, ability to focus and sticking to a
                    scope.
                    <br />
                    <br />
                    Last but not least: Understanding problems is fun and
                    solving them elegantly more so (philosophically speaking,
                    life is a series of problems :). I like sharing the
                    learnings that come with approaching and navigating
                    challenges. With a well-planned approach and some
                    thoughtfulness, every attempt at solving a problem results
                    in something being learned, either by someone individually
                    or together as a team.
                    <br />
                    <br />
                    <Text as="b">And in the end, you always find the bug!</Text>
                  </AccordionPanel>
                </AccordionItem>

                <AccordionItem>
                  <h2>
                    <AccordionButton>
                      <Box flex="1" textAlign="left">
                        <Heading as="h4"> Miscellaneous </Heading>
                      </Box>
                      <AccordionIcon />
                    </AccordionButton>
                  </h2>
                  <AccordionPanel pb={4}>
                    I love books {"("}
                    <Link
                      href="https://books.djuju.tech/play"
                      isExternal
                      color="purple.500"
                    >
                      {"browse my favorites"}
                    </Link>
                    ), I play high-level ultimate for my hometown Berlin{" "}
                    <Link
                      href="https://www.instagram.com/wall_city/"
                      isExternal
                      color="purple.500"
                    >
                      Wall City
                    </Link>{" "}
                    . I often listen to{" "}
                    <Link
                      href="https://soundcloud.com/djujuu/likes"
                      isExternal
                      color="purple.500"
                    >
                      techno
                    </Link>{" "}
                    & deep house when I code and I recently got into playing Go,
                    which is awesome!
                  </AccordionPanel>
                </AccordionItem>
              </Accordion>
            </Box>
          </VStack>
        </Center>
      </Box>
      <br />
    </Layout>
  );
};

export default About;
