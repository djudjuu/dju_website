import styles from "../styles/Home.module.css";
import Layout from "../components/Layout";
import {
  Center,
  Heading,
  Text,
  Link,
  Box,
  VStack,
  Image,
} from "@chakra-ui/react";
import PageLink from "../components/PageLink";

export default function Home() {
  return (
    <Layout>
      <Center>
        <VStack width={["80%", "50%"]}>
          {/* <Image
            src="./images/ballhead.jpg"
            // src="./images/grey_head.JPG"
            alt="me"
            width="200px"
            borderRadius="full"
          /> */}
          <Box textAlign="center">
            <Text fontSize="lg"> </Text>
            <Text>
              Hi! {"I'm "} Julius Faber. I am a Berlin born and raised Software
              Developer with background in Machine Learning & Robotics.
            </Text>
            <br />
            <Text>
              I am in love with python, write solid javascript, and am learning
              to love typescript. I like to build smart-contracts, backends and
              frontends (in that order). I have a soft-spot for testing and
              writing secure, elegant, maintainable code.
            </Text>
            {/* <br /> */}
            {/* <Text>
              {" "}
              I enjoy being the youngest/least-experienced person in a room and
              I will ask a lot of questions.
            </Text> */}
          </Box>
          <Text></Text>
          <br />
          <PageLink href={"/about"} color="purple.500">
            {">>"} read more about me
          </PageLink>
          <Text>or</Text>
          <PageLink href={"/projects"} color="purple.500">
            {">>"} look at projects that I am proud of
          </PageLink>
          <Text>or</Text>
          <Text>
            {" "}
            contact me at <Text as="kbd">djudju(at)proton.me</Text>
          </Text>
          {/* <Text>._.</Text> */}
          <Box h="50px" />
        </VStack>
      </Center>
    </Layout>
  );
}
