import {
  Text,
  Center,
  VStack,
  Box,
  Link,
  UnorderedList,
  ListItem,
} from "@chakra-ui/react";
import Layout from "../components/Layout";
// import Link from "next/link"
const Setup = () => {
  return (
    <Layout>
      <Box>
        <Center>
          <VStack>
            <Text as="i">NOTE: I will add links & update the repo</Text>
            <Text>
              I am currently using
              <UnorderedList>
                <ListItem> ubuntu as my OS </ListItem>
                <ListItem> i3 as my window manager </ListItem>
                <ListItem> VSCode as my go-to editor </ListItem>
                <ListItem> oh-my-zsh as my shell</ListItem>
                <ListItem>
                  {" "}
                  autojump to navigate the shell with speed of thought
                </ListItem>
                <ListItem> Spacemacs as my emacs-distro</ListItem>
                <ListItem> emacs magit-layer for git stuff</ListItem>
                <ListItem>
                  emacs org-mode for organizing my thoughts and writing
                  work-logs
                </ListItem>
                <ListItem> copyq to supercharge my clipboard</ListItem>
                <ListItem>
                  {" "}
                  material-palenight as color-scheme whereever possible :)
                </ListItem>
              </UnorderedList>
            </Text>
            <Box>
              see my config files
              <Link
                color="purple"
                href="https://github.com/djudjuu/configFiles"
                isExternal
              >
                {" repo "}
              </Link>
              (outdated though :/)
            </Box>
          </VStack>
        </Center>
      </Box>
    </Layout>
  );
};

export default Setup;
