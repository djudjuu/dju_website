import {
  Center,
  VStack,
  Heading,
  Drawer,
  Text,
  Accordion,
  ListItem,
  Box,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  UnorderedList,
} from "@chakra-ui/react";
import Layout from "../components/Layout";
import DemoVideo from "../components/DemoVideo";
import FullStackStuff from "../components/FullStackStuff";
import SmartContracts from "../components/SmartContracts";
import HackingForFun from "../components/HackingForFun";
import Research from "../components/Research";
import Writing from "../components/Writing";

const Projects = () => {
  return (
    <Layout>
      <Center>
        <VStack width={["80%", "50%"]}>
          <Box>
            <Text mb={"2"}>
              {" "}
              While I did end up {" 'owning' "}the entire codebase of the
              projects below, they often were collaborative efforts. Therefore
              collaborators are duly given credit below each project
              description.
            </Text>

            <Accordion allowToggle>
              <FullStackStuff />
              <SmartContracts />
              <HackingForFun />
              <Research />
              <Writing />
            </Accordion>
          </Box>
        </VStack>
      </Center>
    </Layout>
  );
};

export default Projects;
